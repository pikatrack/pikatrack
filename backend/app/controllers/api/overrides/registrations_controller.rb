module Api::Overrides
    class RegistrationsController < DeviseTokenAuth::RegistrationsController
        before_action :check_registrations_enabled, only: [:create]

        def new
            render json: {signups_enabled: ApplicationSetting.where(
                code: 'signups_enabled', bool_value: true
            ).exists?}, status: :ok
        end

        def check_registrations_enabled
            if ApplicationSetting.where(code: 'signups_enabled', bool_value: false).exists?
                render json: {error: 'signups disabled by admin'}, status: :forbidden
            end
        end
    end
end