class Api::Admin::ApplicationSettingsController < ApplicationController
    before_action :authorize_admin

    def index
        @application_settings = ApplicationSetting.all
    end

    def update
        ApplicationSetting.find_by(code: params[:code]).update(application_settings_params)
    end

    def authorize_admin
        authorize! :manage, :all
    end

    def application_settings_params
        params.require(:application_settings).permit(:text_value, :bool_value, :number_value)
    end
end
