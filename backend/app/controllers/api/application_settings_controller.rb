class Api::ApplicationSettingsController < ApplicationController
    SETTINGS_WHITELIST = ['signups_enabled', 'mapbox_api_key'].freeze
    def index
        @application_settings = ApplicationSetting.where(code: SETTINGS_WHITELIST)
    end
end
