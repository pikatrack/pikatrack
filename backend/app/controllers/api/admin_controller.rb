class Api::AdminController < ApplicationController
    before_action :authorize_admin

    def dashboard_stats
        @stats = {
            users: User.count,
            activities: Activity.count,
            sections: Section.count
        }
        render 'api/admin/dashboard_stats', formats: [:json]
    end

    def authorize_admin
        authorize! :manage, :all
    end
end
