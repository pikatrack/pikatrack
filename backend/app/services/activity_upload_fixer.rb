require 'haversine'

# Processes activity files. End result is used on the website and the original
# copy is kept in case in need of reprocessing but never used directly

class ActivityUploadFixer
    def initialize(activity)
        @activity_file_temp = Tempfile.new
        @activity_file_temp.binmode
        @activity_file_temp.write activity.original_activity_log_file.download

        # Without this line, reads of this file in other methods show up blank. I don't know why. Investigate this
        @activity_file_temp.read 
        @activity = activity
    end

    def fix_file
        file_type = @activity.original_activity_log_file.blob.content_type
        if file_type == 'application/vnd.ant.fit'
            fit_to_gpx
        elsif file_type == 'application/gpx+xml'
        else
            raise "Unsupported file type #{file_type}"
        end

        join_trkseg
        crop_privacy_zone
        return @activity_file_temp
    end

    def fit_to_gpx
        GPSBabel.convert({
             input: {
                 format: 'garmin_fit',
                 file: @activity_file_temp.path
             },
             output: {
                 format: 'gpx',
                 file: @activity_file_temp.path
             }
         })
    end

    # GPX supports multiple track segments. Some logging apps create a new segment after they have been paused and resumed.
    # We don't care about this extra data so lets group it in to one track segment.
    def join_trkseg
        new_gpx = GPX::GPXFile.new()
        track = GPX::Track.new()
        new_gpx.tracks << track
        segment = GPX::Segment.new()
        track.segments << segment

        old_gpx = GPX::GPXFile.new(gpx_file: @activity_file_temp.path)
        old_gpx.tracks.first.segments.each do |old_segment|
            segment.points += old_segment.points
        end

        new_gpx.write(@activity_file_temp.path)
    end

    def crop_privacy_zone
        gpx = GPX::GPXFile.new(gpx_file: @activity_file_temp.path)
        points = []
        gpx.tracks.first.segments.first.points.each do |point|
            points << point unless in_range(point)
        end
        gpx.tracks.first.segments.first.points = points
        gpx.write(@activity_file_temp.path)
    end

    def in_range(point)
        @activity.user.privacy_zones.each do |pz|
            pz_point = RGeo::GeoJSON.encode(pz.random_in_range)['coordinates']
            distance = Haversine.distance(pz_point[1], pz_point[0], point.lat, point.lon).to_meters
            return true if distance < pz.range
        end
        false
    end
end
