class SectionEffort < ApplicationRecord
    belongs_to :activity
    belongs_to :section
    belongs_to :user

    before_save :check_achievement

    enum achievement: {pr: 0, second_pr: 1, third_pr: 2, new_section: 3}

    def check_achievement
        efforts = SectionEffort.where(user_id: self.user_id, section_id: self.section_id).limit(3).order(:time)
        if efforts.count == 0
            self.achievement = :new_section
        elsif self.time < efforts[0].time
            self.achievement = :pr
        elsif efforts[1] && self.time < efforts[1].time
            self.achievement = :second_pr
        elsif efforts[2] && self.time < efforts[2].time
            self.achievement = :third_pr
        end

    end
end
