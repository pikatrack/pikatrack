class AddAdminToUsers < ActiveRecord::Migration[5.2]
  def change
      add_column :users, :admin, :boolean, required: true, default: false
  end
end
