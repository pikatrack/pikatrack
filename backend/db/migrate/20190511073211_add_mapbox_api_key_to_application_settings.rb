class AddMapboxApiKeyToApplicationSettings < ActiveRecord::Migration[5.2]
  def change
    ApplicationSetting.create(code:"mapbox_api_key", text_value: '')
  end
end
