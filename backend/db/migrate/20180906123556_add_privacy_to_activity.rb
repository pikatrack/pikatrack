class AddPrivacyToActivity < ActiveRecord::Migration[5.2]
    def change
        add_column :activities, :privacy, :integer, null: false, default: 0
    end
end
