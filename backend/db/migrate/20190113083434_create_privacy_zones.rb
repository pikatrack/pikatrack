class CreatePrivacyZones < ActiveRecord::Migration[5.2]
  def change
    create_table :privacy_zones do |t|
      t.references :user
      t.st_point :original_point, geographic: true, null: false
      t.st_point :random_in_range, geographic: true, null: false
      t.integer :range, null: false
      t.timestamps
    end
  end
end
