class AddEnableSignupsApplicationSetting < ActiveRecord::Migration[5.2]
  def change
      ApplicationSetting.create(code: "signups_enabled", bool_value: true)
  end
end
