class MakeCodeNotNull < ActiveRecord::Migration[5.2]
  def change
      change_column_null :application_settings, :code, false
  end
end
