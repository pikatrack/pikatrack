class CreateApplicationSettings < ActiveRecord::Migration[5.2]
    def change
        create_table :application_settings do |t|
            t.string :code
            t.boolean :bool_value
            t.text :text_value
            t.float :number_value
            t.timestamps
        end
    end
end
