FactoryBot.define do
    factory :privacy_zone do
        association :user, factory: :user
    end
end
