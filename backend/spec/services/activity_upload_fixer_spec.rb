require 'rails_helper'

RSpec.describe "Activity upload fixer" do
    it 'converts fit files to gpx' do
        activity = FactoryBot.create(:activity_fit_file)
        converted = ActivityUploadFixer.new(activity).fix_file
        expect(IO.readlines(converted.path)[0]).to eq("<?xml version=\"1.0\"?>\n")
    end

    it 'crops start and end if cointained in privacy zone' do
        user = FactoryBot.create(:user)
        activity = FactoryBot.create(:activity_fox_gpx, user: user)
        FactoryBot.create(:privacy_zone, {
            random_in_range: "POINT(138.8381540 -34.8772670)",
            original_point: "POINT(138.8381540 -34.8772670)",
            range: 400,
            user: user
        })
        processed = ActivityUploadFixer.new(activity).fix_file
        gpx = GPX::GPXFile.new(gpx_file: processed.path)

        # Points before privacy zone crop 1018
        expect(gpx.tracks.first.segments.first.points.count).to eq(920)
    end

    it 'joins files with multiple trksegs in to one' do
        activity = FactoryBot.create(:activity_split_trkseg)
        processed = ActivityUploadFixer.new(activity).fix_file
        gpx = GPX::GPXFile.new(gpx_file: processed.path)

        expect(gpx.tracks.first.segments.count).to eq(1)
    end
end
