Rails.application.routes.draw do
    namespace 'api' do
        require 'sidekiq/web'
        mount_devise_token_auth_for 'User', at: 'auth', controllers: {
            registrations:  'api/overrides/registrations'
        }
        devise_scope :api_user do
            get '/auth/new' => 'overrides/registrations#new'
        end

        mount Sidekiq::Web => '/sidekiq'
        get 'application_settings' => 'application_settings#index'
        get 'admin/' => 'admin#dashboard_stats'
        namespace 'admin' do
            get '/users' => 'users#index'
            post '/users/:id/lock' => 'users#lock_user'
            post '/users/:id/unlock' => 'users#unlock_user'
            get '/application_settings' => 'application_settings#index'
            post '/application_settings' => 'application_settings#update'
        end
        get '/profile' => 'profile#show'
        post '/profile/' => 'profile#update'
        scope :profile do
            resources :privacy_zone, except: [:update]
        end


        get '/user/:id' => 'user#show'

        get 'section/:id' => 'section#show'
        post 'section/' => 'section#create'

        get 'section_efforts' => 'section_effort#index'

        resources :activity
        post '/activity/:id/recompute' => 'activity#recompute'

        get '/feed' => 'feed#index'

        get '/user/:id/activities' => 'activity#index'
    end
end
