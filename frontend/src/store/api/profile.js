import Vuex from 'vuex'
import Vue from 'vue'
import Vapi from 'vuex-rest-api'

Vue.use(Vuex)

const profile = new Vapi({
    baseURL: '/api',
    state: {
        profile: []
    }
}).get({
    action: 'show_profile',
    property: 'profile',
    path: '/profile.json'
}).getStore({
    createStateFn: true
})

export default {
    state: profile.state,
    mutations: profile.mutations,
    actions: profile.actions,
    getters: profile.getters
}
