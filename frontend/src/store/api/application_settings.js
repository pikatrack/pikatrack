import Vuex from 'vuex'
import Vue from 'vue'
import Vapi from 'vuex-rest-api'

Vue.use(Vuex)

const application_settings = new Vapi({
    baseURL: '/api',
    state: {
        application_settings: []
    }
}).get({
    action: 'application_settings_index',
    property: 'application_settings',
    path: '/application_settings.json'
}).getStore({
    createStateFn: true
})

export default {
    state: application_settings.state,
    mutations: application_settings.mutations,
    actions: application_settings.actions,
    getters: application_settings.getters
}
