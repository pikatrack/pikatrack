import Vuex from 'vuex'
import Vue from 'vue'
import Vapi from 'vuex-rest-api'

Vue.use(Vuex)

const privacyZones = new Vapi({
    baseURL: '/api',
    state: {
        privacyZones: []
    }
}).get({
    action: 'get_privacy_zones',
    property: 'privacyZones',
    path: '/profile/privacy_zone.json'
}).delete({
    action: 'delete_privacy_zone',
    property: 'privacyZones',
    path: ({id}) => `/profile/privacy_zone/${id}`
}).getStore({
    createStateFn: true
})

export default {
    state: privacyZones.state,
    mutations: privacyZones.mutations,
    actions: privacyZones.actions,
    getters: privacyZones.getters
}
