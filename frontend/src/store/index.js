import Vue from 'vue'
import Vuex from 'vuex'

import applicationSettings from './api/application_settings'
import profile from './api/profile'
import signup from './api/signup'
import privacyZones from './api/privacyZones'
import user from './api/user'
import activity from './api/activity'
import section from './api/section'
import sectionEfforts from './api/sectionEfforts'
import feed from './api/feed'
import admin from './api/admin'
import adminUsers from './api/admin/users'
import adminApplicationSettings from './api/admin/application_settings'

Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        applicationSettings,
        profile,
        signup,
        privacyZones,
        user,
        activity,
        section,
        sectionEfforts,
        feed,
        admin,
        adminUsers,
        adminApplicationSettings
    }
})
