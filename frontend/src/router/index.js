import Router from 'vue-router'

import Home from '../components/Home'
import ProfileIndex from '../components/profile/Show'
import ProfileEdit from '../components/profile/Edit'
import UserShow from '../components/user/Show'
import PrivacyZoneIndex from '../components/profile/privacy_zone/Index'
import PrivacyZoneCreate from '../components/profile/privacy_zone/Create'
import ActivityShow from '../components/activities/Show'
import ActivityIndex from '../components/activities/Index'
import ActivityCreate from '../components/activities/Create'
import SectionShow from '../components/sections/Show'
import SectionCreate from '../components/sections/Create'
import Login from '../components/Login'
import Signup from '../components/Signup'
import Contact from '../components/Contact'
import Admin from '../components/admin/Index'
import AdminUserIndex from '../components/admin/users/Index'
import AdminApplicationSettingsIndex from '../components/admin/application_settings/Index'

export default new Router({
    routes: [
        {path: '/', component: Home},
        {path: '/activity/new', component: ActivityCreate},
        {path: '/activity/:id', component: ActivityShow},
        {path: '/user/:id/activities', component: ActivityIndex},
        {path: '/profile', component: ProfileIndex},
        {path: '/profile/edit', component: ProfileEdit},
        {path: '/profile/privacy_zone', component: PrivacyZoneIndex},
        {path: '/profile/privacy_zone/new', component: PrivacyZoneCreate},
        {path: '/user/:id', component: UserShow},
        {path: '/section/:id', component: SectionShow},
        {path: '/section/create/:id', component: SectionCreate},
        {path: '/login', component: Login},
        {path: '/signup', component: Signup},
        {path: '/contact', component: Contact},
        {path: '/admin', component: Admin},
        {path: '/admin/users', component: AdminUserIndex},
        {path: '/admin/application_settings', component: AdminApplicationSettingsIndex}
    ],
    mode: 'history'
})
